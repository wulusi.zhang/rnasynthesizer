﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NucleotideIdentifier : MonoBehaviour
{
    [SerializeField]
    GameObject AssignedNuc;

    public int nucID;

    public int connectID = -1;

    [SerializeField]
    List<GameObject> connectedNucleotides = new List<GameObject>();

    [SerializeField]
    GameObject CorrectID, FalseID;

    GameObject SelectedAnimator;

    bool isLinked;
    // Start is called before the first frame update
    void Start()
    {
        nucID = Random.Range(0, AssignedNuc.transform.childCount);
        CheckSelfID(nucID);
        DeActivateLinkers();
    }

    void DeActivateLinkers()
    {
        for (int i = 0; i < connectedNucleotides.Count; i++)
        {
            connectedNucleotides[i].gameObject.SetActive(false);
        }
    }

    void CheckSelfID(float idValue)
    {
        switch (idValue)
        {
            case 0:
                ActivateAssigned(0);
                break;
            case 1:
                ActivateAssigned(1);
                break;
            case 2:
                ActivateAssigned(2);
                break;
            case 3:
                ActivateAssigned(3);
                break;
        }
    }

    void ActivateAssigned(int value)
    {
        for (int i = 0; i < AssignedNuc.transform.childCount; i++)
        {
            AssignedNuc.transform.GetChild(i).gameObject.SetActive(false);
        }

        //AssignedNuc.transform.GetChild(value).gameObject.SetActive(true);

        SelectedAnimator = AssignedNuc.transform.GetChild(value).gameObject;

        SelectedAnimator.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ArmController nucleotideID = collision.GetComponent<ArmController>();

        if (null != nucleotideID && !isLinked)
        {
            isLinked = true;
            connectID =
            nucleotideID.GetArmID();
            DeActivateLinkers();
            connectedNucleotides[connectID].SetActive(true);
            CheckNucID();
        }
    }

    public void CheckNucID()
    {
        Animator NucAnimator = SelectedAnimator.GetComponent<Animator>();

        switch (nucID)
        {
            //Nuc A
            case 0:
                switch (connectID)
                {
                    //Linkers
                    case -1:
                        //No Linkers
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 0:
                        //A to A
                        Debug.Log("False A to A Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 1:
                        //A to C
                        Debug.Log("False A to C Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 2:
                        //A to U
                        Debug.Log("Correct A to U Pair");
                        GameManager.instance.IncreaseScore(connectID);
                        GameManager.instance.IncreaseStreak();
                        CorrectID.SetActive(true);
                        NucAnimator.SetTrigger("Good");
                        break;
                    case 3:
                        //A to G
                        Debug.Log("False A to G Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                }
                break;
            //Nuc C
            case 1:
                switch (connectID)
                {
                    case -1:
                        //No Linkers
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 0:
                        //C to A
                        Debug.Log("False C to A Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 1:
                        //C to C
                        Debug.Log("False C to C Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 2:
                        //C to U
                        Debug.Log("False C to U Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 3:
                        //C to G
                        Debug.Log("Correct C to G Pair");
                        GameManager.instance.IncreaseScore(connectID);
                        GameManager.instance.IncreaseStreak();
                        CorrectID.SetActive(true);
                        NucAnimator.SetTrigger("Good");
                        break;
                }
                break;

            //Nuc T
            case 2:
                switch (connectID)
                {
                    case -1:
                        //No Linkers
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 0:
                        //T to A
                        Debug.Log("Correct T to A Pair");
                        GameManager.instance.IncreaseScore(connectID);
                        GameManager.instance.IncreaseStreak();
                        CorrectID.SetActive(true);
                        NucAnimator.SetTrigger("Good");
                        break;
                    case 1:
                        //T to C
                        Debug.Log("False T to C Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 2:
                        //T to U
                        Debug.Log("False T to T Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 3:
                        //T to G
                        Debug.Log("False T to G Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                }
                break;
            //Nuc G
            case 3:
                switch (connectID)
                {
                    case -1:
                        //No Linkers
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 0:
                        //G to A
                        Debug.Log("False G to A Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 1:
                        //G to C
                        Debug.Log("Correct G to C Pair");
                        GameManager.instance.IncreaseScore(connectID);
                        GameManager.instance.IncreaseStreak();
                        CorrectID.SetActive(true);
                        NucAnimator.SetTrigger("Good");
                        break;
                    case 2:
                        //G to U
                        Debug.Log("False G to T Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                    case 3:
                        //G to G
                        Debug.Log("False G to G Pair");
                        GameManager.instance.ResetScoreStreak();
                        FalseID.SetActive(true);
                        NucAnimator.SetTrigger("Fail");
                        break;
                }
                break;
        }
    }
}
