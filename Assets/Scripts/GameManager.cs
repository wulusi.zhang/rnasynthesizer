﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> Arms = new List<GameObject>();

    [SerializeField]
    private GameObject Nucleotide;

    public float lastArmIndex;

    public Transform NucleotideSpawnPost;

    [SerializeField]
    KeyCode ArmKey1, ArmKey2, ArmKey3, ArmKey4;

    [SerializeField]
    bool canSpawn = false;

    [SerializeField]
    [Range(0, 0.5f)]
    float nucleotideSpd;

    [SerializeField]
    [Range(0, 2)]
    float spawnDelay;

    [SerializeField]
    [Range(0, 20)]
    float armSpeed;

    [SerializeField]
    private bool CanMoveArm;

    [SerializeField]
    private List<GameObject> Nucleotides = new List<GameObject>();

    [SerializeField]
    int currentScoreStreak;


    [SerializeField]
    TextMeshProUGUI Score, Streak, ChainText;

    [SerializeField]
    List<GameObject> SynthChains = new List<GameObject>();

    float timer = 0;

    [SerializeField]
    GameObject RNAMolecule;

    [SerializeField]
    GameObject LastRNAMolecule1;

    [SerializeField]
    GameObject MenuScreen;

    [SerializeField]
    GameObject Credits;

    [SerializeField]
    int currentScore;

    [SerializeField]
    GameObject ScoreText;

    [SerializeField]
    GameObject QuitPrompt;

    [SerializeField]
    GameObject EnoughButton;

    [SerializeField]
    GameObject Guide;

    #region Singleton Logic
    public static GameManager instance = null;
    private void SingletonCheck()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion


    private void Awake()
    {
        SingletonCheck();
        CanMoveArm = true;
        canSpawn = false;
        ResetScoreStreak();
        Score.gameObject.SetActive(false);
        ScoreText.SetActive(false);
        EnoughButton.SetActive(false);
    }

    public List<GameObject> GetNucleotides()
    {
        return Nucleotides;
    }

    public void ToggleCredits()
    {
        if(!Credits.activeInHierarchy)
        {
            Credits.SetActive(true);
        }
    }

    public void DisableCredits()
    {
        if(Credits != null)
        {
            Credits.SetActive(false);
        }
    }
    public bool SetArmBool(bool value)
    {
        CanMoveArm = value;
        return CanMoveArm;
    }

    public bool GetArmBool()
    {
        return CanMoveArm;
    }
    public bool SetNucleotideBool(bool value)
    {
        CanMoveArm = value;
        return CanMoveArm;
    }

    public void StartGame() 
    {
        MenuScreen.SetActive(false);
        canSpawn = true;
        Score.gameObject.SetActive(true);
        ScoreText.SetActive(true);
        StartCoroutine(SpawnNucleotides());
        EnoughButton.SetActive(true);
        Guide.SetActive(true);
    }


    private IEnumerator SpawnNucleotides()
    {
        while (canSpawn)
        {
            yield return new WaitForSeconds(spawnDelay);
            AddNucleotide();
        }
    }

    public void AddNucleotide()
    {
        if (Nucleotide != null)
        {
            GameObject NewNuc = Instantiate(Nucleotide,
                NucleotideSpawnPost);
            if (!Nucleotides.Contains(NewNuc))
            {
                Nucleotides.Add(NewNuc);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(ArmKey1)
            && CanMoveArm)
        {
            Arms[0].GetComponent<ArmController>().SetToMove();
            lastArmIndex = 0;
            CanMoveArm = false;
        }

        if (Input.GetKeyDown(ArmKey2)
            && CanMoveArm)
        {
            Arms[1].GetComponent<ArmController>().SetToMove();
            lastArmIndex = 1;
            CanMoveArm = false;
        }

        if (Input.GetKeyDown(ArmKey3)
            && CanMoveArm)
        {
            Arms[2].GetComponent<ArmController>().SetToMove();
            lastArmIndex = 2;
            CanMoveArm = false;
        }

        if (Input.GetKeyDown(ArmKey4)
            && CanMoveArm)
        {
            Arms[3].GetComponent<ArmController>().SetToMove();
            lastArmIndex = 3;
            CanMoveArm = false;
        }
    }

    void FixedUpdate()
    {
        //Nucleotide spawn and movement speed should
        //be independant of the arm speed
        //if (!CanMoveArm)
        {
            GetMoveMentSpd();
        }
    }
    public void GetMoveMentSpd()
    {
        for (int i = 0; i < Nucleotides.Count; i++)
        {
            Nucleotides[i].transform.position +=
                Vector3.right * nucleotideSpd;
        }
    }
    public float GetArmSpeed()
    {
        return Arms[(int)lastArmIndex].
            GetComponent<ArmController>().GetTimer();
    }

    private void OnValidate()
    {
        for (int i = 0; i < Arms.Count; i++)
        {
            Arms[i].GetComponent<ArmController>().SetLerpSpeed(armSpeed);
        }
    }

    public void IncreaseScore(int ChainID)
    {
        currentScore++;
        RaiseArmSpeed();
        SpawnRNAStrand(ChainID);
        Score.text = currentScore.ToString();
        Score.gameObject.GetComponent<Animator>().SetTrigger("Play");
    }

    public void IncreaseStreak()
    {
        currentScoreStreak++;
        RaiseNucRate();
        if (ChainText.gameObject.activeInHierarchy == false)
            ChainText.gameObject.SetActive(true);

        ChainText.gameObject.GetComponent<Animator>().SetTrigger("Play");

        if (Streak.gameObject.activeInHierarchy == false)
        {
            Streak.gameObject.SetActive(true);
        }
        Streak.text = currentScoreStreak.ToString();

        //if (Streak.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
        Streak.gameObject.GetComponent<Animator>().SetTrigger("Play");
    }

    public void ResetScoreStreak()
    {
        currentScoreStreak = 0;
        DecreaseNucRate();
        if (Streak.gameObject.activeInHierarchy)
        {
            Streak.text = currentScoreStreak.ToString();
            Streak.gameObject.SetActive(false);
        }
        ChainText.gameObject.SetActive(false);
    }

    void RaiseArmSpeed()
    {
        armSpeed += 0.1f;
        armSpeed = Mathf.Clamp(armSpeed, 0, 10f);
    }

    void RaiseNucRate()
    {
        //[SerializeField]
        //[Range(0, 0.5f)]
        nucleotideSpd += 0.005f;
        nucleotideSpd = Mathf.Clamp(nucleotideSpd, 0.05f, 0.5f);

        //[SerializeField]
        //[Range(0, 2)]
        spawnDelay -= 0.025f;
        spawnDelay = Mathf.Clamp(spawnDelay, 0.05f, 3f);
    }

    void DecreaseNucRate()
    {
        nucleotideSpd -= 0.02f;
        nucleotideSpd = Mathf.Clamp(nucleotideSpd, 0.05f, 0.5f);

        //[SerializeField]
        //[Range(0, 2)]
        spawnDelay += 0.05f;
        spawnDelay = Mathf.Clamp(spawnDelay, 0.05f, 3f);
    }

    void AssignRNAColour(GameObject RNA, int colorID)
    {
        RNA.transform.GetChild(colorID).gameObject.SetActive(true);
    }

    void SpawnRNAStrand(int ChainID)
    {
        timer = 0;
        if (SynthChains.Count != 0)
        {
            //First transform child is the strand anchor
            GameObject Anchor = SynthChains[0].transform.GetChild(0).gameObject;
            GameObject RNANuc = Instantiate(RNAMolecule, Anchor.transform.parent);

            AssignRNAColour(RNANuc, ChainID);

            RNANuc.GetComponent<Rigidbody2D>().isKinematic = true;
            StartCoroutine(ExtendRNAStrandRight(Anchor));

            if (LastRNAMolecule1 == null)
            {
                //Connect Nucleotide to anchor
                RNANuc.GetComponent<DistanceJoint2D>().connectedBody = Anchor.GetComponent<Rigidbody2D>();
                LastRNAMolecule1 = RNANuc;
            }
            else
            {
                LastRNAMolecule1.GetComponent<Rigidbody2D>().isKinematic = false;
                RNANuc.GetComponent<DistanceJoint2D>().connectedBody = LastRNAMolecule1.GetComponent<Rigidbody2D>();
                LastRNAMolecule1 = RNANuc;
            }
        }
    }

    IEnumerator ExtendRNAStrandRight(GameObject RNAStrand)
    {
        while (timer < 0.75f)
        {
            timer += Time.deltaTime;
            RNAStrand.transform.localPosition += Vector3.right * Time.deltaTime;
            yield return null;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ActivateQuitGamePrompt()
    {
        QuitPrompt.SetActive(true);
    }
}
