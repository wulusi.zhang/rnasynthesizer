﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NucleotideDeleter : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {   
       if(GameManager.instance.GetNucleotides().Contains(collision.gameObject))
        {
            GameManager.instance.GetNucleotides().Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }
}
