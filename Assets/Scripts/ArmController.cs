﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmController : MonoBehaviour
{
    [SerializeField]
    Transform EndPos;

    [SerializeField]
    Transform StartPos;

    [SerializeField]
    float LerpSpeed;

    float timer;

    [SerializeField]
    bool canMove = false;
    [SerializeField]
    bool canInput = true;
    [SerializeField]
    bool moveBack = false;

    [SerializeField]
    int armID;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = StartPos.position;
        timer = 0;
    }
    // Update is called once per frame
    void Update()
    {
        //if (GameManager.instance.GetArmBool())
        //{
        //    canMove = true;
        //    GameManager.instance.AddNucleotide();
        //    GameManager.instance.SetArmBool(false);
        //}
    }

    public int GetArmID() 
    {
        return armID;
    }
    public void SetToMove()
    {
        canMove = true;
    }
    private void FixedUpdate()
    {
        if (canMove)
        {
            moveBack = false;
            MoveArm();
            CheckDistance();
        }

        if (moveBack)
        {
            canMove = false;
            MoveArmBack();
            CheckDistanceBack();
        }
    }
    void MoveArm()
    {
        timer += Time.deltaTime * LerpSpeed;
        this.transform.position = Vector3.Lerp(StartPos.position, EndPos.position, timer);
    }

    public void SetLerpSpeed(float value)
    {
        LerpSpeed = value;
    }

    public float GetTimer()
    {
        return timer;
    }

    void MoveArmBack()
    {
        timer += Time.deltaTime * LerpSpeed * 2;
        this.transform.position = Vector3.Lerp(EndPos.position, StartPos.position, timer);
    }


    void CheckDistance()
    {
        float distance = Vector3.Distance(this.transform.position, EndPos.transform.position);

        if (distance < 0.1f)
        {
            canMove = false;
            moveBack = true;
            timer = 0;
        }
    }

    void CheckDistanceBack()
    {
        float distance = Vector3.Distance(this.transform.position, StartPos.transform.position);

        if (distance < 0.1f)
        {
            moveBack = false;
            GameManager.instance.SetArmBool(true);
            timer = 0;
        }
    }
}
